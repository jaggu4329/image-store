set FLASK_SECRET_KEY=ILovePythonDev
set FLASK_ENV=development
set FLASK_DEBUG=true
set DATABASE_NAME=image-store-dev.db
set HOST=http://127.0.0.1:5000/
set FLASK_APP=app.py